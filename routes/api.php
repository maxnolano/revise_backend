<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Api\ReviseController;

Route::group(['namespace' => 'Api', 'prefix' => 'auth'], function(){
	Route::post('/register', [AuthController::class, 'register']);
	Route::post('/login', [AuthController::class, 'login']);
	Route::get('/user', [AuthController::class, 'user']);
	Route::post('/logout', [AuthController::class, 'logout']);
});

Route::group(['namespace' => 'Api', 'prefix' => 'revise'], function(){
    Route::post('/revise-files', [ReviseController::class, 'reviseFiles'])->middleware('auth:api');
    Route::post('/revise-azs-files', [ReviseController::class, 'reviseAzsFiles'])->middleware('auth:api');
    Route::post('/revise-bank-files', [ReviseController::class, 'reviseBankFiles'])->middleware('auth:api');
	Route::get('/revise-redis', [ReviseController::class, 'reviseRedis'])->middleware('auth:api');
});