<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmOrdersRetalixCancelledCompleted extends Model
{
    use HasFactory;

    protected $table = 'crm_orders_retalix_cancelled_completed';

    protected $fillable = [
        'id',
        'order_id',
        'litre',
        'store_id',
        'cancel_reason',
        'ext_order_id',
        'ext_date',
        'create_date',
        'status',
    ];
}
