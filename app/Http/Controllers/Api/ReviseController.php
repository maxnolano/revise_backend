<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use App\Services\Revise\ReviseService;
use Illuminate\Support\Facades\Redis;

class ReviseController extends ApiController
{
    /**
     * Revise files
     * @param Request $request
     * @return Object
     */
    public function reviseFiles(Request $request): array {
        $response = (new ReviseService($request))->reviseFiles();
        return $this->result($response);
    }

    /**
     * Revise azs files
     * @param Request $request
     * @return Object
     */
    public function reviseAzsFiles(Request $request): array {
        $response = (new ReviseService($request))->reviseAzsFiles();
        return $this->result($response);
    }

    /**
     * Revise bank files
     * @param Request $request
     * @return Object
     */
    public function reviseBankFiles(Request $request): array {
        $response = (new ReviseService($request))->reviseBankFiles();
        return $this->result($response);
    }

    /**
     * Revise redis
     * @param Request $request
     * @return Object
     */
    public function reviseRedis(Request $request): Object {
        return $this->result(Redis::ping());
    }
}