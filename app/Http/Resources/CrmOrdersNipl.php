<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\User as UserResource;

class CrmOrdersNipl extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'store_id' => $this->store_id,
            'pump_number' => $this->pump_number,
            'product_price' => $this->product_price,
            'order_payment_type' => $this->order_payment_type,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'st_title' => $this->st_title,
            'product_id' => $this->product_id,
            'total_order_amt' => $this->total_order_amt,
            'external_id' => $this->external_id,
        ];
    }
}
