<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\User as UserResource;

class CrmOrdersRetalix extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'store_id' => $this->store_id,
            'pump_number' => $this->pump_number,
            'product_price' => $this->product_price,
            'payment_type' => $this->payment_type,
            'status' => $this->status,
            'create_date' => $this->create_date,
            'modify_date' => $this->modify_date,
            'system_type' => $this->system_type,
            'st_title' => $this->st_title,
            'nipl_order_id' => $this->nipl_order_id,
            'product_id' => $this->product_id,
            'total_order_amt' => $this->total_order_amt,
        ];
    }
}
