<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Carbon\Carbon;
use Excel;
use Mail;
use App\Exports\ReviseExport;
use Illuminate\Support\Facades\Storage;
use DB;
use Illuminate\Support\Facades\Log;

class HandleFilesRevisingJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $request;

    public $failOnTimeout = false;

    public $timeout = 60000000;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $filename = Carbon::now('Asia/Almaty')->format('Y-m-d_H-i-s'). '.xlsx';

        $this->request['request_id'] = $filename;

        Excel::store(new ReviseExport($this->request), $filename, 'public_uploads', null, [
            'visibility' => 'public',
        ]);

        $data = null;
        $data['mail'] = $this->request['mail_adrs'];
        $data['filename'] = $filename;

        $file = public_path('reports/'.$filename);

        if($this->request['mail_default'] == true){
            $data['mail'] = ['alibek@smartgas.global', 'maxnolano@gmail.com'];
        }
    
        Mail::send('mail.mail-pattern', $data, function($message)use($data, $file) {
            $message->to($data['mail'])
                    ->subject('Отчет по сверке.');

            $message->attach($file);
        });

        DB::connection('mysql_local')->table('temp_sgg_revise')->where('request_id', '=', $filename)->delete();

        DB::connection('mysql_local')->table('temp_bank_revise')->where('request_id', '=', $filename)->delete();

        DB::connection('mysql_local')->table('temp_azs_revise')->where('request_id', '=', $filename)->delete();

        if(file_exists(storage_path('app/public/public/' . $this->request['nameSgg']))){
            Log::info('1 case service');
            unlink(storage_path('app/public/public/' . $this->request['nameSgg']));
        }elseif(file_exists(storage_path('app/public/' . $this->request['nameSgg']))){
            Log::info('2 case service');
            unlink(storage_path('app/public/' . $this->request['nameSgg']));
        }else{
            Log::info('3 case service');
        }

        if(file_exists(storage_path('app/public/public/' . $this->request['nameBank']))){
            Log::info('4 case service');
            unlink(storage_path('app/public/public/' . $this->request['nameBank']));
        }elseif(file_exists(storage_path('app/public/' . $this->request['nameBank']))){
            Log::info('5 case service');
            unlink(storage_path('app/public/' . $this->request['nameBank']));
        }else{
            Log::info('6 case service');
        }

        if(file_exists(storage_path('app/public/public/' . $this->request['nameAzs']))){
            Log::info('7 case service');
            unlink(storage_path('app/public/public/' . $this->request['nameAzs']));
        }elseif(file_exists(storage_path('app/public/' . $this->request['nameAzs']))){
            Log::info('8 case service');
            unlink(storage_path('app/public/' . $this->request['nameAzs']));
        }else{
            Log::info('9 case service');
        }

        // unlink(storage_path('app/public/' . $this->request['nameSgg']));
        // unlink(storage_path('app/public/' . $this->request['nameBank']));
        // unlink(storage_path('app/public/' . $this->request['nameAzs']));
    }
}
