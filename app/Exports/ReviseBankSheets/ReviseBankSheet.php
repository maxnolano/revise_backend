<?php

namespace App\Exports\ReviseBankSheets;

use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithTitle;
use Illuminate\Support\Facades\Log;
use Excel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class ReviseBankSheet implements FromCollection, WithHeadings, WithEvents, WithTitle
{
    private Array $request;

    public function __construct(Array $request)
    {
        $this->request = $request;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {

        $files_revised_status = '';

        $request_id = $this->request['request_id'];
        $resultBank = null;
        $resultSgg = null;

        if(file_exists(storage_path('app/public/public/' . $this->request['nameBank']))){
            Log::info('1 case');
            $resultBank = Excel::toArray([], storage_path('app/public/public/' . $this->request['nameBank']))[0];
        }elseif(file_exists(storage_path('app/public/' . $this->request['nameBank']))){
            Log::info('2 case');
            $resultBank = Excel::toArray([], storage_path('app/public/' . $this->request['nameBank']))[0];
        }else{
            Log::info('3 case');
        }

        if(file_exists(storage_path('app/public/public/' . $this->request['nameSgg']))){
            Log::info('7 case');
            $resultSgg = Excel::toArray([], storage_path('app/public/public/' . $this->request['nameSgg']))[0];
        }elseif(file_exists(storage_path('app/public/' . $this->request['nameSgg']))){
            Log::info('8 case');
            $resultSgg = Excel::toArray([], storage_path('app/public/' . $this->request['nameSgg']))[0];
        }else{
            Log::info('9 case');
        }

        // $resultBank = Excel::toArray([], storage_path('app/public/public/' . $this->request['nameBank']))[0];
        // $resultSgg = Excel::toArray([], storage_path('app/public/public/' . $this->request['nameSgg']))[0];

        $counterSgg = 0;
        $column_index = -1;
        foreach($resultSgg as $res){
            if($counterSgg == 0){
                if($res[10] == 'Номер транзакции'){
                    $column_index = 10;
                }else{
                    $column_index = 8;
                }
            }
            if($counterSgg > 0){
                if($column_index == 10){
                    DB::connection('mysql_local') 
                    ->table('temp_sgg_revise')->insert([ 
                        'order_num' => $res[0],
                        'check_num' => $res[1],
                        'azs_title' => $res[2],
                        'paid_sum' => $res[3],
                        'transaction_date' => $res[4],
                        'status' => $res[$column_index - 1],
                        'transaction_num' => $res[$column_index],
                        'written_off_amount' => $res[11],
                        'request_id' => $request_id
                    ]);
                }else{
                    DB::connection('mysql_local') 
                    ->table('temp_sgg_revise')->insert([ 
                        'order_num' => $res[0],
                        'check_num' => $res[1],
                        'azs_title' => $res[2],
                        'paid_sum' => $res[3],
                        'transaction_date' => $res[4],
                        'transaction_num' => $res[$column_index],
                        'request_id' => $request_id
                    ]);
                }
            }
            $counterSgg++;
        }

        $counterBank = 0;
        foreach($resultBank as $res){
            if($counterBank > 1){
                $date = Carbon::parse($res[1])->toDateTime();
                DB::connection('mysql_local') 
                ->table('temp_bank_revise')->insert([ 
                    'payment_id' => $res[0], 
                    'date' => $date,
                    'sum' => $res[3],
                    'request_id' => $request_id
                ]);
            }
            $counterBank++;
        }

        $query = null;
        $query2 = null;
        $query3 = null;
        $query4 = null;

        // $query = DB::connection('mysql_local') 
        // ->table('temp_sgg_revise as smr')
        // ->leftJoin('temp_bank_revise as bnk', 'smr.transaction_num', '=', 'bnk.payment_id')
        // ->leftJoin('temp_azs_revise as azs', 'smr.check_num', '=', 'azs.check_num')
        // ->where(function($item) use ($column_index){
        //     $item->whereColumn(($column_index == 10) ? 'smr.written_off_amount' : 'smr.paid_sum','<>', 'azs.discount_sum')
        //         // ->whereRaw("TIMESTAMPDIFF(SECOND, smr.transaction_date, azs.date) <= 86400")
        //         // ->whereRaw("TIMESTAMPDIFF(SECOND, smr.transaction_date, azs.date) >= 0")
        //         // ->where(function($item2) {
        //         //     $item2->where('azs.card_num', '=', '0048005000000049')
        //         //         ->orWhere('azs.card_num', '=', '0');
        //         // });
        // })
        // ->select(
        //     'smr.azs_title as Наименование АЗС (Смартгаз)',
        //     'smr.transaction_date as Дата транзакции (Смартгаз)',
        //     'bnk.date as Дата (Банк)',
        //     'smr.order_num as Номер заказа (Смартгаз)',
        //     'bnk.payment_id as ID платежа (Банк)',
        //     'smr.paid_sum as Оплаченная сумма (Смартгаз)',
        //     'smr.written_off_amount as Списанная сумма (Смартгаз)',
        //     'bnk.sum as Сумма (Банк)',
        // );

        $query2 = DB::connection('mysql_local') 
        ->table('temp_sgg_revise as smr')
        ->leftJoin('temp_bank_revise as bnk', 'smr.transaction_num', '=', 'bnk.payment_id')
        // ->leftJoin('temp_azs_revise as azs', 'smr.check_num', '=', 'azs.check_num')
        ->where(function($item) use ($column_index){
            $item->whereColumn(($column_index == 10) ? 'smr.written_off_amount' : 'smr.paid_sum', '<>', 'bnk.sum');
                // ->whereRaw("TIMESTAMPDIFF(SECOND, smr.transaction_date, azs.date) <= 86400")
                // ->whereRaw("TIMESTAMPDIFF(SECOND, smr.transaction_date, azs.date) >= 0")
                // ->where(function($item2) {
                //     $item2->where('azs.card_num', '=', '0048005000000049')
                //         ->orWhere('azs.card_num', '=', '0');
                // });
        })
        ->select(
            // 'smr.azs_title as Наименование АЗС (Смартгаз)',
            'smr.transaction_date as Дата транзакции (Смартгаз)',
            'bnk.date as Дата (Банк)',
            'smr.order_num as Номер заказа (Смартгаз)',
            'bnk.payment_id as ID платежа (Банк)',
            'smr.status as Статус (Смартгаз)',
            'smr.paid_sum as Оплаченная сумма (Смартгаз)',
            'smr.written_off_amount as Списанная сумма (Смартгаз)',
            'bnk.sum as Сумма (Банк)',
        );

        // $query3 = DB::connection('mysql_local') 
        // ->table('temp_sgg_revise as smr')
        // ->leftJoin('temp_bank_revise as bnk', 'smr.transaction_num', '=', 'bnk.payment_id')
        // ->leftJoin('temp_azs_revise as azs', 'smr.check_num', '=', 'azs.check_num')
        // ->where(function($item) {
        //     $item->whereColumn('bnk.sum', '<>', 'azs.discount_sum')
        //         // ->whereRaw("TIMESTAMPDIFF(SECOND, smr.transaction_date, azs.date) <= 86400")
        //         // ->whereRaw("TIMESTAMPDIFF(SECOND, smr.transaction_date, azs.date) >= 0")
        //         // ->where(function($item2) {
        //         //     $item2->where('azs.card_num', '=', '0048005000000049')
        //         //         ->orWhere('azs.card_num', '=', '0');
        //         // });
        // })
        // ->select(
        //     'smr.azs_title as Наименование АЗС (Смартгаз)',
        //     'smr.transaction_date as Дата транзакции (Смартгаз)',
        //     'bnk.date as Дата (Банк)',
        //     'smr.order_num as Номер заказа (Смартгаз)',
        //     'bnk.payment_id as ID платежа (Банк)',
        //     'smr.paid_sum as Оплаченная сумма (Смартгаз)',
        //     'smr.written_off_amount as Списанная сумма (Смартгаз)',
        //     'bnk.sum as Сумма (Банк)',
        // );

        // $query4 = DB::connection('mysql_local') 
        // ->table('temp_sgg_revise as smr')
        // ->leftJoin('temp_bank_revise as bnk', 'smr.transaction_num', '=', 'bnk.payment_id')
        // // ->leftJoin('temp_azs_revise as azs', 'smr.check_num', '=', 'azs.check_num')
        // ->where(function($item) use ($column_index){
        //     $item->whereColumn(($column_index == 10) ? 'smr.written_off_amount' : 'smr.paid_sum', '<>', 'bnk.sum')
        //         ->whereColumn('bnk.sum', '<>', 'azs.discount_sum')
        //         ->whereColumn('azs.discount_sum', '<>', ($column_index == 10) ? 'smr.written_off_amount' : 'smr.paid_sum')
        //         // ->whereRaw("TIMESTAMPDIFF(SECOND, smr.transaction_date, azs.date) <= 86400")
        //         // ->whereRaw("TIMESTAMPDIFF(SECOND, smr.transaction_date, azs.date) >= 0")
        //         // ->where(function($item2) {
        //         //     $item2->where('azs.card_num', '=', '0048005000000049')
        //         //         ->orWhere('azs.card_num', '=', '0');
        //         // });
        // })
        // ->select(
        //     'smr.azs_title as Наименование АЗС (Смартгаз)',
        //     'smr.transaction_date as Дата транзакции (Смартгаз)',
        //     'bnk.date as Дата (Банк)',
        //     'smr.order_num as Номер заказа (Смартгаз)',
        //     'bnk.payment_id as ID платежа (Банк)',
        //     'smr.paid_sum as Оплаченная сумма (Смартгаз)',
        //     'smr.written_off_amount as Списанная сумма (Смартгаз)',
        //     'bnk.sum as Сумма (Банк)',
        // )
        // ->union($query)
        // ->union($query2)
        // ->union($query3)
        // ->get();

        return $query2->get();
    }

    public function headings(): array
    {
        return ["Дата транзакции (Смартгаз)", "Дата (Банк)", "Номер заказа (Смартгаз)", "ID платежа (Банк)", 'Статус (Смартгаз)', "Оплаченная сумма (Смартгаз)", "Списанная сумма (Смартгаз)", 'Сумма (Банк)'];// , 'time diff'
    }

    public function registerEvents() : array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->getDelegate()->getStyle('A:H')
                                ->getAlignment()
                                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

                $event->sheet->getDelegate()->getColumnDimension('A')->setWidth(35);
                $event->sheet->getDelegate()->getColumnDimension('B')->setWidth(35);
                $event->sheet->getDelegate()->getColumnDimension('C')->setWidth(35); 
                $event->sheet->getDelegate()->getColumnDimension('D')->setWidth(35); 
                $event->sheet->getDelegate()->getColumnDimension('E')->setWidth(35);
                $event->sheet->getDelegate()->getColumnDimension('F')->setWidth(35);
                $event->sheet->getDelegate()->getColumnDimension('G')->setWidth(35);
                $event->sheet->getDelegate()->getColumnDimension('H')->setWidth(35);
                // $event->sheet->getDelegate()->getColumnDimension('I')->setWidth(35);
                // $event->sheet->getDelegate()->getColumnDimension('J')->setWidth(35);
                // $event->sheet->getDelegate()->getColumnDimension('K')->setWidth(35);
                // $event->sheet->getDelegate()->getColumnDimension('L')->setWidth(35);
                // $event->sheet->getDelegate()->getColumnDimension('M')->setWidth(35);
            }
        ];
    }

    public function title(): string
    {
        return 'Сверка';
    }
}