<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use App\Exports\ReviseBankSheets\ReviseBankSheet;

class ReviseBankExport implements WithMultipleSheets
{
    use Exportable;

    private Array $request;

    public function __construct(Array $request)
    {
        $this->request = $request;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];

        $sheets[] = new ReviseBankSheet($this->request);

        return $sheets;
    }
}