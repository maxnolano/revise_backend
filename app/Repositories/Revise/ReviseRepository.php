<?php


namespace App\Repositories\Revise;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Query\Builder;
use App\Repositories\Interfaces\ReviseRepositoryInterface;
use GuzzleHttp\{Client, Exception\GuzzleException, Psr7\Request};
use Illuminate\Support\Facades\Storage;
use File;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Excel;
// use Maatwebsite\Excel\Facades\Excel as Excel;
use Illuminate\Support\Facades\Log;

class ReviseRepository implements ReviseRepositoryInterface
{
    // GuzzleHttp client example
    protected Client $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * Revise files
     * @param Array $request
     * @return Object
     */
    public function reviseFiles(Array $request): array
    {   
        $files_revised_status = 'revised';

        return ['revised_status' => [$files_revised_status]];
    }

    /**
     * Revise azs files
     * @param Array $request
     * @return Object
     */
    public function reviseAzsFiles(Array $request): array
    {   
        $files_revised_status = 'revised';

        return ['revised_status' => [$files_revised_status]];
    }

    /**
     * Revise bank files
     * @param Array $request
     * @return Object
     */
    public function reviseBankFiles(Array $request): array
    {   
        $files_revised_status = 'revised';

        return ['revised_status' => [$files_revised_status]];
    }
}