<?php

namespace App\Services\Revise;

use App\Repositories\Revise\ReviseRepository;
use App\Repositories\Interfaces\ReviseRepositoryInterface;
use App\Services\BaseService;
use Illuminate\Http\Request;
use GuzzleHttp\{Client, Exception\GuzzleException, Psr7\Request as ClientRequest};
use App\Jobs\HandleFilesRevisingJob;
use App\Jobs\HandleAzsFilesRevisingJob;
use App\Jobs\HandleBankFilesRevisingJob;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Queue;

/**
 * Service for revises
 */
class ReviseService extends BaseService
{
    // Incoming request
    private Request $request;

    // Repository for revises
    private ReviseRepositoryInterface $reviseRepository;

    // GuzzleHttp client example
    protected Client $client;

    /**
     * Class constructor
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        // Request initialization
        $this->request = $request;

        // EditRepository initialization
        $this->reviseRepository = new ReviseRepository();

        $this->client = new Client();
    }

    /**
     * Revise files
     * @return Object
     */
    public function reviseFiles(): array
    {
        try{
            $request_params = [];

            $request_params['fileBank'] = $this->request->file('fileBank');
            $request_params['nameBank'] = $this->request->nameBank;

            $request_params['fileAzs'] = $this->request->file('fileAzs');
            $request_params['nameAzs'] = $this->request->nameAzs;

            $request_params['fileSgg'] = $this->request->file('fileSgg');
            $request_params['nameSgg'] = $this->request->nameSgg;

            $date_to_save = Carbon::now('Asia/Almaty')->format('Y-m-d_H-i-s_');

            $fileBank = $request_params['fileBank'];
            $nameBank = $date_to_save . $request_params['nameBank'];

            $fileAzs = $request_params['fileAzs'];
            $nameAzs = $date_to_save . $request_params['nameAzs'];

            $fileSgg = $request_params['fileSgg'];
            $nameSgg = $date_to_save . $request_params['nameSgg'];

            $fileBank->storeAs('public', $nameBank);
            $fileAzs->storeAs('public', $nameAzs);
            $fileSgg->storeAs('public', $nameSgg);

            $request_params = [];

            $request_params['nameBank'] = $nameBank;

            $request_params['nameAzs'] = $nameAzs;

            $request_params['nameSgg'] = $nameSgg;

            $request_params['mail_default'] = $this->request->mailDefault;
            $request_params['mail_adrs'] = $this->request->mailAdrs;

            // $files_revised_status = dispatch(new HandleFilesRevisingJob($request_params));

            Queue::push(new HandleFilesRevisingJob($request_params));

            $files_revised_status = 'revising';

            $result = ['revised_status' => [$files_revised_status]];

            return $this->result(['data' => [$result]]);

        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Revise azs files
     * @return Object
     */
    public function reviseAzsFiles(): array
    {
        try{
            $request_params = [];

            $request_params['fileAzs'] = $this->request->file('fileAzs');
            $request_params['nameAzs'] = $this->request->nameAzs;

            $request_params['fileSgg'] = $this->request->file('fileSgg');
            $request_params['nameSgg'] = $this->request->nameSgg;

            $date_to_save = Carbon::now('Asia/Almaty')->format('Y-m-d_H-i-s_');

            $fileAzs = $request_params['fileAzs'];
            $nameAzs = $date_to_save . $request_params['nameAzs'];

            $fileSgg = $request_params['fileSgg'];
            $nameSgg = $date_to_save . $request_params['nameSgg'];

            $fileAzs->storeAs('public', $nameAzs);
            $fileSgg->storeAs('public', $nameSgg);

            $request_params = [];

            $request_params['nameAzs'] = $nameAzs;

            $request_params['nameSgg'] = $nameSgg;

            $request_params['mail_default'] = $this->request->mailDefault;
            $request_params['mail_adrs'] = $this->request->mailAdrs;

            // $files_revised_status = dispatch(new HandleFilesRevisingJob($request_params));

            Queue::push(new HandleAzsFilesRevisingJob($request_params));

            $files_revised_status = 'revising';

            $result = ['revised_status' => [$files_revised_status]];

            return $this->result(['data' => [$result]]);

        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Revise bank files
     * @return Object
     */
    public function reviseBankFiles(): array
    {
        try{
            $request_params = [];

            $request_params['fileBank'] = $this->request->file('fileBank');
            $request_params['nameBank'] = $this->request->nameBank;

            $request_params['fileSgg'] = $this->request->file('fileSgg');
            $request_params['nameSgg'] = $this->request->nameSgg;

            $date_to_save = Carbon::now('Asia/Almaty')->format('Y-m-d_H-i-s_');

            $fileBank = $request_params['fileBank'];
            $nameBank = $date_to_save . $request_params['nameBank'];

            $fileSgg = $request_params['fileSgg'];
            $nameSgg = $date_to_save . $request_params['nameSgg'];

            $fileBank->storeAs('public', $nameBank);
            $fileSgg->storeAs('public', $nameSgg);

            $request_params = [];

            $request_params['nameBank'] = $nameBank;

            $request_params['nameSgg'] = $nameSgg;

            $request_params['mail_default'] = $this->request->mailDefault;
            $request_params['mail_adrs'] = $this->request->mailAdrs;

            // $files_revised_status = dispatch(new HandleFilesRevisingJob($request_params));

            Queue::push(new HandleBankFilesRevisingJob($request_params));

            $files_revised_status = 'revising';

            $result = ['revised_status' => [$files_revised_status]];

            return $this->result(['data' => [$result]]);

        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }
}