<?php

namespace App\Console\Commands;

use App\Jobs\HandleAzsFilesRevisingJob;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;

class ReviseAzsFilesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'revise:ReviseAzsFiles';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Revise azs files through redis queue jobs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $file = [1, 2, 3, 4, 5, 6];

        // Log::info('doing some command');

        dispatch(new HandleAzsFilesRevisingJob($file));// ->onQueue('revise')
        // Queue::push(new HandleFilesRevisingJob($file));
    }
}
