<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\Individual\IndividualCronService;

class IndividualDailyReportCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inddailyreport:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends individual reports on a daily basis interval to responding addressees';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $date = date('Y-m-d', strtotime("-1 days"));
        $request = [];
        $request['start'] = $date.' 00:00:00';
        $request['end'] = $date.' 23:59:59';
        (new IndividualCronService($request))->report();
    }
}
